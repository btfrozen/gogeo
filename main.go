package main

import (
	"fmt"
	"github.com/oschwald/geoip2-golang"
	"log"
	"net"
	"net/http"
	"encoding/json"
	"os"
)

// Struct for response
type JsonResponse struct {
	CityNameRu string
	RegionNameRu string
	RegionIsoCode string
	CountryIsoCode string
	CountryNameRu string
}

type CityWrapper struct {
	record *geoip2.City
}

func (cw *CityWrapper) getRegionName() string {
	if (len(cw.record.Subdivisions)>0) {
		return cw.record.Subdivisions[0].Names["ru"]
	}
	return ""
}

func (cw *CityWrapper) getRegionIso() string {
	if (len(cw.record.Subdivisions)>0) {
		return cw.record.Subdivisions[0].IsoCode
	}
	return ""
}

func (cw *CityWrapper) getCityName() string {
	return cw.record.Country.Names["ru"];
}



//
func main() {
	db, err := geoip2.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		defer func() {
			if r := recover(); r != nil {
				fmt.Fprintf(w, "%s", r)
			}
		}()

		ip := net.ParseIP(r.URL.Query().Get("q"))
		record, err := db.City(ip)
		if err != nil {
			fmt.Fprintf(w, err.Error())
			return
		}

		cw := CityWrapper{record}

		rs := JsonResponse{
			CityNameRu: record.City.Names["ru"],
			RegionNameRu: cw.getRegionName(),
			RegionIsoCode: cw.getRegionIso(),
			CountryNameRu: cw.getCityName(),
			CountryIsoCode: record.Country.IsoCode,
		}

		js, err := json.Marshal(rs)

		if err != nil {
			fmt.Fprintf(w, err.Error())
		}

		if (r.URL.Query().Get("c") != "") {
			fmt.Fprintf(w, "%s(%s)", r.URL.Query().Get("c"), string(js))
			return
		}

		fmt.Fprintf(w, string(js))
	}) // set router
	err = http.ListenAndServe(":9099", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}